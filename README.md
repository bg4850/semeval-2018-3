# Irony detection in English tweets

Irony is a  figure of speech (or in some cases writing) in which the intended meaning is the opposite of that expressed by the words used; usually taking the form of sarcasm or ridicule in which laudatory expressions are used to imply condemnation or contempt (taken from Oxford English Dictionary). 
Meaning that line between irony and sarcasm is blurred and detecting irony also encapsulates detecting sarcasm. Both are often used in reviews and any other kind of commentary on social media sites. Especially on Twitter because users have only limited number of characters to use. With irony you can often express a lot with a small number of words. Recognizing irony is very important in identifying general public's opinion. 

## Data

All of the training data is taken from https://competitions.codalab.org/competitions/17468.
3834 of tweets are given in the following format:  
tweet_index		is_ironic		tweet_text
Additionally, is_ironic column is extended in additional file to type of irony (1 (ironic by clash), 2 (situational irony), 3 (other irony), 0 (non-ironic))

There are four metrics used:
- Accuracy = (number of correctly predicted instances) / (number of instances)  
- Precision = (number of correctly predicted instances) / (number of predicted labels)  
- Recall = (number of correctly predicted labels) / (number of labels in the gold standard)  
- F-score = 2 * (precision * recall) / (precision + recall)

## Related work
Ellen Riloff et al. (2013) studied the role of negative/ situations with positive sentiments like "I love going to work during holidays" and vice versa. They built corpus of sarcastic tweets and with positive sentiment words/seeds (for example: "love") and learned the negative situation phrases. Repeating the procedure for negative sentiment seeds. This seems like a smart idea to be used for irony by clash.  
Dmitry Davidov et al. (2010) used a semi-supervised classification algorithm with database of sarcastic sentences scaled from 1 to 5 based on sarcasm level. Then they used tweets and reviews to build patterns based high frequency words and content words. These patterns were then used to build feature vectors based on pattern matching and classified sentences using k-nearest neighbor algorithm. This strategy could prove useful for irony detection as it could help solve the question of classifying different kinds of irony.  
Christine Liebrecht et al. (2013) also used machine learning approach. They stripped all the punctuation and build uni-, bi- and trigrams as features. This approach seems most naive and we don't intend to use it in our problem.

## Additional corpora
Corpora "Second Release of the American National Corpus Frequency Data" (http://www.anc.org/data/anc-second-release/frequency-data/) - provides the number of occurrences of a word in the written and spoken in American National Corpus.  
Polarity corpora (http://www.cs.cornell.edu/people/pabo/movie-review-data/) because irony often contains polarity of sentiment of the words used.
We also make use of emojis in determining tweet sentiment. List of all emoji symbols and their sentiment score was obtained from http://kt.ijs.si/data/Emoji_sentiment_ranking/.

## Initial idea for solving
Detecting irony based on the fact that often there is a sharp discrepancy between the expected sentimentality and an actual one (for example positive sentiment with negative situation).
Apart from sentiment polarity, several other features including tokenized word count, 2-mer and 3-mer count, character flooding and punctuation flooding are introduced to create feature vectors for each tweet.
With all features created, classification is done with support vector machines (SVM).

## Results
Model was tested with 5-fold cross-validation over 3834 tweets with ironic to non-ironic ratio close to 50%.
Since our idea was to detect irony using sentiment polarity, we wanted to compare the results obtained with sentiment and flooding features to those we achieved using only tf-idf.
We also compared our results with purely random classifier. Results are presented in table below:

|Classifier     | Accuracy      | Precision     | Recall  | F-score |
|--------------:| ------------- |:-------------:| -------:|--------:|
| Random        | 0.5023        | 0.5008        | 0.5034  | 0.5020  |
| Td-idf        | 0.6299        | 0.6374        | 0.5971  | 0.6166  |
| Td-idf + NLP  | 0.6846        | 0.6813        | 0.6902  | 0.6857  |

Since some types of irony are impossible to detect solely from linguistic properties of tweets, we can not expect to achieve close to 100% accuracy.
To put our results in perspective, accuracy achieved in related works mentioned above just above 70%.
We can see from the table above that SVM with only tf-idf vectors as features resulted in 63% classification accuracy.
Adding word word 2-mers and 3-mers, character 3-mets, flags for character and punctuation flooding together with overall sentiment and sentiment polarity score resulted in 68% accuracy.

## Possible future improvements
In the first part our main focus was on feature engineering. With lexical and sentiment features now created, the plan is to try featurizing some semantic properties as well.
On the other hand we have only tried classification with SVM, which leaves us a lot of room for experimentation with various classification algorithms (e.g. logistic regression, decision trees...).

## Installation guide
### Prerequisites
- Python 3.6
- Libraries: [ntlk](http://www.nltk.org/), [scikit-learn](http://scikit-learn.org/), [scipy](https://www.scipy.org/)
### Instructions
Clone repository and run.
*Warning:* running this code may take a while (10min +)
