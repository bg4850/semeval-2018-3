import csv

with open("data/emoji_sentiment.csv","r") as f:
    with open("data/vader_lexicon.txt","a") as printF:
        csvFile = csv.reader(f, delimiter=';', quotechar='\"')
        for line in csvFile: #9-sentiment, 10-varianca, 11-ime
            printF.write('\t'.join([(':'+'_'.join(line[11].lower().split(" "))+':'),str(float(line[9])*4),str(float(line[10])*4)])+"\t[]\n")