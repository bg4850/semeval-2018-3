﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace EmojiParse
{
    class Program
    {
        static void Main(string[] args)
        {
            HttpClient client = new HttpClient();

            string html = client.GetStringAsync("http://kt.ijs.si/data/Emoji_sentiment_ranking/").Result;
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            var table = doc.GetElementbyId("myTable");
            string csv = "";

            //loop through table rows
            foreach (var tr in table.SelectNodes("//tr").Skip(1))
            {
                //loop through table cells
                foreach (var td in tr.SelectNodes("./td"))
                {
                    //if td does not contain any text its sentiment bar - parse this for sentiment score
                    if (td.InnerText.Trim() == "")
                    {
                        var score = td.SelectNodes("./div/div/div").FirstOrDefault(x =>
                            x.GetAttributeValue("title", "").Contains("sentiment score"))
                            ?.GetAttributeValue("title","")
                            ?.Replace("sentiment score: ","");

                        csv += score?.Replace("&#177","") + ";";
                      
                    }
                    //regular text
                    else
                    {
                        //emoji simbols contain ";" which mess with csv structure. Delete them from html and append at the end
                        csv += td.InnerText.Replace(";","").Trim() + ";";
                    }
                }

                //we dont need semicolon at the end of line
                csv = csv.TrimEnd(';') + "\n";
            }


            File.WriteAllText(@"D:\emojisentiment.csv",csv);
        }
      
    }
}
