#!/usr/bin/env python3
# Semantic
from nltk.sentiment.vader import SentimentIntensityAnalyzer, negated
from afinn import Afinn

# From example
import nltk
from nltk.tokenize import TweetTokenizer
from nltk import word_tokenize, pos_tag, ne_chunk
from nltk.tag import untag, str2tuple, tuple2str
from nltk.chunk import tree2conllstr, conllstr2tree, conlltags2tree, tree2conlltags
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.datasets import dump_svmlight_file
from sklearn import metrics
from nltk.stem import WordNetLemmatizer
import numpy as np
import logging
import string
from scipy.sparse import csr_matrix, hstack
import codecs
import re
import time
from gensim.models import Word2Vec
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle
from sklearn import svm, datasets
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import StratifiedKFold
from scipy import interp



logging.basicConfig(level=logging.INFO)


class IronyDetTwitter:

    ####################### PREPROCESSING ##############################
    def __init__(self, fp):
        '''
        Loads the dataset .txt file with label-tweet on each line and parses the dataset.
        :param fp: filepath of dataset
        :return:
            corpus: dict of tweets (tweet_id: tweet_content)
        '''
        self.corpus = {}
        self.y = list()
        self.text = list()
        with open(fp, 'rt', encoding="utf8") as data_in:
            for line in data_in:
                if not line.lower().startswith("tweet index"):  # discard first line if it contains metadata
                    line = line.rstrip().split("\t")
                    self.corpus[int(line[0]) - 1] = line[2]  # subtract 1 to start indexing with 0
                    self.text.append(line[2])
                    self.y.append(int(line[1]))

        self.y = np.array(self.y)

        now = time.time()
        self.ie_preprocess()
        print('Preprocessing: ' + '%0.2f' % (time.time()-now))

        self.sid = SentimentIntensityAnalyzer(lexicon_file='data/vader_lexicon.txt')

    def parse_dataset_old(self, fp):
        '''
        Loads the dataset .txt file with label-tweet on each line and parses the dataset.
        :param fp: filepath of dataset
        :return:
            corpus: list of tweet strings of each tweet.
            y: list of labels
        '''
        y = list()
        corpus = list()
        #dict = {}
        with open(fp, 'rt', encoding="utf8") as data_in:
            for line in data_in:
                if not line.lower().startswith("tweet index"):  # discard first line if it contains metadata
                    line = line.rstrip().split("\t")
                    y.append(int(line[1]))  # index of a tweet
                    corpus.append(line[2])  # tweet content

        return corpus, y

    def ie_preprocess(self):
        tokenizer = TweetTokenizer(preserve_case=False, reduce_len=True, strip_handles=True)
        self.tokenized = {}
        self.pos_tagged = {}
        #self.ner_tree = {}
        #self.iob_tagged = {}

        for id,tweet in self.corpus.items():
            tweet = re.sub(r"http\S+", "", tweet) # remove links
            tokenized = tokenizer.tokenize(tweet)
            tagged_tokens = pos_tag(tokenized)
            #ner_tree = ne_chunk(tagged_tokens)
            #iob_tagged = tree2conlltags(ner_tree)

            self.tokenized[id] = tokenized
            self.pos_tagged[id] = tagged_tokens
            #self.ner_tree[id] = ner_tree
            #self.iob_tagged[id] = iob_tagge

        # print(self.tokenized[2])
        # print(self.pos_tagged[2])


    ####################### FEATURIZING ##############################

    ### SEMANTIC

    def word2vecVectorizer(self, tweet, n):
        return np.array([np.mean([self.w2v[w] for w in tweet if w in self.w2v]
                                    or [np.zeros(n)], axis=0)])


    def semantic_features(self, n=200):
        res = csr_matrix((len(self.tokenized), n))

        sents = self.corpus.values()
        model = Word2Vec(sents, size=n)
        self.w2v = dict(zip(model.wv.index2word, model.wv.syn0))

        for id,tweet in self.tokenized.items():
            res[id] = self.word2vecVectorizer(tweet, n)

        return res

    #### SENTIMENT

    def connect_negated(self, tokenized_tweet):
        if not negated(tokenized_tweet):
            return tokenized_tweet
        else:
            res = list()
            negated_part = ""
            in_negated = False
            for word in tokenized_tweet:
                if negated([word]): # negated funkcija sprejme seznam stringov
                    in_negated = True
                    negated_part = word
                elif in_negated and negated([word]): # dvojna negacija
                    res.extend(negated_part.split(" "))
                    in_negated = False
                elif in_negated and self.sid.polarity_scores(word)['compound'] != 0: # konec negacije
                    negated_part += ' ' + word
                    in_negated = False
                    res.append(negated_part)
                elif in_negated: # smo se zmerom v negaciji
                    negated_part += ' ' + word
                else:
                    res.append(word)
            return res

    def sentiment_features(self):
        res = list()
        for id, tweet in self.tokenized.items():
            tweet = self.connect_negated(tweet)
            scores = [self.sid.polarity_scores(word)['compound'] for word in tweet]
            if not scores:
                res.append([0,0,0])
                continue
            #print(scores)
            max_score = max(scores)
            min_score = min(scores)

            overall_score = self.sid.polarity_scores(self.text[id])['compound']
            difference = max_score - min_score
            polarity = int((np.sign(max_score) - np.sign(min_score)) == 2)

            res.append([overall_score, difference, polarity])

        return csr_matrix(res)

    #### LEXICAL

    def character_flooding(self):
        #res = [[0] for x in range(len(self.tokenized))]
        res = csr_matrix((len(self.tokenized), 1))
        regex = re.compile(r'(\w)\1\1')

        for id, tweet in self.corpus.items():
            is_flooded = 0
            match = regex.search(tweet)
            if match:
                is_flooded = 1
            res[id] = is_flooded

        return res

    def punctuation_flooding(self):
        #res = [[0] for x in range(len(self.tokenized))]
        res = csr_matrix((len(self.tokenized), 1))
        regex = re.compile(r'([.\?#@+,<>%~`!$^&\(\):;]|[.\?#@+,<>%~`!$^&\(\):;]\s)\1+')

        for id, tweet in self.corpus.items():
            is_flooded = 0
            match = regex.search(tweet)
            if match:
                is_flooded = 1
            res[id] = is_flooded

        return res

    def kmers(self, s, k=3):
        for i in range(len(s) - k + 1):
            yield tuple(s[i:i + k])

    def kmers_featurize(self, k=2, elem="word"):
        i = 0
        kmer_dict = {}

        # build dict
        for id, tweet in self.tokenized.items():
            if elem == "char":
                tweet = self.text[id]

            for kmer in list(self.kmers(tweet, k=k)):
                if kmer not in kmer_dict:
                    kmer_dict[kmer] = i
                    i += 1

        # featurize
        res = csr_matrix((len(self.tokenized), len(kmer_dict)))

        for id, tweet in self.tokenized.items():
            if elem == "char":
                tweet = self.text[id]

            for kmer in list(self.kmers(tweet, k=k)):
                res[id , kmer_dict[kmer]] = 1  # row is id of a tweet, column is value in kmer_dict for this kmer

        return res



    ### FEATURIZING

    def featurize(self):
        '''
        Tokenizes and creates TF-IDF BoW vectors.
        :param corpus: A list of strings each string representing document.
        :return: X: A sparse csr matrix of TFIDF-weigted ngram counts.
        '''

        tokenizer = TweetTokenizer(preserve_case=False, reduce_len=True, strip_handles=True).tokenize
        vectorizer = TfidfVectorizer(strip_accents="unicode", analyzer="word", tokenizer=tokenizer, stop_words="english")
        X = csr_matrix(vectorizer.fit_transform(self.text))
        #print(vectorizer.get_feature_names()) # to manually check if the tokens are reasonable

        now = time.time()
        wordUnimersDict = self.kmers_featurize(k=1)
        wordDimersDict = self.kmers_featurize(k=2)
        charTrimersDict = self.kmers_featurize(k=3, elem="char")
        #charQuamersDict = self.kmers_featurize(k=4, elem="char")
        print('Featurize: ' + '%0.2f' % (time.time() - now))

        X = hstack([X, wordUnimersDict])
        X = hstack([X, wordDimersDict])
        X = hstack([X, charTrimersDict])
        # X = hstack([X, charQuamersDict])

        now = time.time()
        floodingCharDict = self.character_flooding()
        floodingPuncDict = self.punctuation_flooding()
        print('Flooding: ' + '%0.2f' % (time.time() - now))

        X = hstack([X, floodingCharDict])
        X = hstack([X, floodingPuncDict])

        now = time.time()
        sent = self.sentiment_features()
        print('Sentimality: ' + '%0.2f' % (time.time() - now))
        X = hstack([X, sent])


        seman = self.semantic_features()
        print('Semantic: ' + '%0.2f' % (time.time() - now))
        X = hstack([X, seman])

        return X


if __name__ == "__main__":
    # Experiment settings
    print('--- Started ----')
    # Dataset: SemEval2018-T4-train-taskA.txt or SemEval2018-T4-train-taskB.txt
    DATASET_FP = "./data/SemEval2018-T3-train-taskA.txt"

    TASK = "A"  # Define, A or B
    FNAME = './predictions-task' + TASK + '.txt'
    PREDICTIONSFILE = open(FNAME, "w")

    K_FOLDS = 5  # 10-fold crossvalidation
    CLF = LinearSVC()  # the default, non-parameter optimized linear-kernel SVM

    # Loading dataset and featurised simple Tfidf-BoW model
    idt = IronyDetTwitter(DATASET_FP)
    # corpus, y = idt.parse_dataset_old(DATASET_FP)
    #------------ custom code

    X = idt.featurize()

    class_counts = np.asarray(np.unique(idt.y, return_counts=True)).T.tolist()

    # SVM with created features - currently uses only tokenized words.
    predicted = cross_val_predict(CLF, X, idt.y, cv=K_FOLDS)
    randpred = np.random.randint(2, size= len(predicted))

    acc = metrics.accuracy_score(idt.y, randpred)
    prec = metrics.precision_recall_fscore_support(idt.y, randpred, average='binary', pos_label=1)

    print("Random classifier:")
    print('Accuracy', acc, '| Precision', prec[0], '| Recall', prec[1], '| F-score', prec[2])
    # Modify F1-score calculation depending on the task
    if TASK.lower() == 'a':
        acc = metrics.accuracy_score(idt.y,predicted)
        prec = metrics.precision_recall_fscore_support(idt.y, predicted, average='binary', pos_label=1)
    elif TASK.lower() == 'b':
        score = metrics.f1_score(idt.y, predicted, average="macro")

    print('Full')
    print('Accuracy', acc, '| Precision', prec[0], '| Recall', prec[1], '| F-score', prec[2])
    for p in predicted:
        PREDICTIONSFILE.write("{}\n".format(p))
    PREDICTIONSFILE.close()

    cv = StratifiedKFold(n_splits=K_FOLDS)
    classifier = svm.SVC(kernel='linear', probability=True)
    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)

    i = 0
    for train, test in cv.split(X, idt.y):
        print(idt.y[train])
        probas_ = classifier.fit(X.tocsr()[train], idt.y[train]).predict_proba(X.tocsr()[test])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(idt.y[test], probas_[:, 1])
        tprs.append(interp(mean_fpr, fpr, tpr))
        tprs[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs.append(roc_auc)
        plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='ROC fold %d (AUC = %0.2f)' % (i, roc_auc))

        i += 1
    plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r',
             label='Luck', alpha=.8)

    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs)
    plt.plot(mean_fpr, mean_tpr, color='b',
             label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)

    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$ 1 std. dev.')

    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Irony decection ROC')
    plt.legend(loc="lower right")
    plt.show()




# http://www.nltk.org/howto/sentiment.html

